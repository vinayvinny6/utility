drop table global_settings_entity if exists;

create table global_settings_entity (
    id bigint generated by default as identity,
    category varchar(255),
    default_value varchar(255),
    max_value integer not null,
    min_value integer not null,
    name varchar(255),
    type varchar(255),
    primary key (id)
);