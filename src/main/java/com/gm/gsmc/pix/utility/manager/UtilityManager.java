package com.gm.gsmc.pix.utility.manager;

import com.gm.gsmc.pix.api.service.PixException;
import com.gm.gsmc.pix.model.settings.SettingDescriptor;
import com.gm.gsmc.pix.model.vehicle.MakeModelYear;
import com.gm.gsmc.pix.utility.jpa.GlobalSettingsRepository;
import com.gm.gsmc.pix.utility.jpa.MMYRepository;
import com.gm.gsmc.pix.utility.jpa.model.GlobalSettingsEntity;
import com.gm.gsmc.pix.utility.jpa.model.MMYEntity;
import com.gm.gsmc.pix.utility.translator.MMYTranslator;
import com.gm.gsmc.pix.utility.translator.SettingsTranslator;
import com.gm.gsmc.pix.utility.translator.reverse.ReverseMMYTranslator;
import com.gm.gsmc.pix.utility.translator.reverse.ReverseSettingsTranslator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class UtilityManager {

    private final GlobalSettingsRepository globalSettingsRepo;
    private final MMYRepository mmyRepo;
    private final SettingsTranslator settingsTranslator;
    private final MMYTranslator mmyTranslator;
    private final ReverseMMYTranslator reverseMMYTranslator;
    private final ReverseSettingsTranslator reverseSettingsTranslator;

    @Autowired
    public UtilityManager(GlobalSettingsRepository globalSettingsRepo, MMYRepository mmyRepo, SettingsTranslator settingsTranslator, MMYTranslator mmyTranslator,
                          ReverseMMYTranslator reverseMMYTranslator, ReverseSettingsTranslator reverseSettingsTranslator){
        this.globalSettingsRepo = globalSettingsRepo;
        this.mmyRepo = mmyRepo;
        this.settingsTranslator = settingsTranslator;
        this.mmyTranslator = mmyTranslator;
        this.reverseMMYTranslator = reverseMMYTranslator;
        this.reverseSettingsTranslator = reverseSettingsTranslator;
    }

/*GET Methods*/
    public List<SettingDescriptor> getAllDefaultSettings() {
        Iterable<GlobalSettingsEntity> allDefaultSettings = globalSettingsRepo.findAll();
        if(allDefaultSettings == null){
            return new ArrayList<>();
        }
        return settingsTranslator.translate(allDefaultSettings);
    }

    public SettingDescriptor getDefaultSetting(String name) {
        GlobalSettingsEntity defaultSetting = globalSettingsRepo.findOneByName(name);
        if(defaultSetting == null){
            return new SettingDescriptor();
        }
        return settingsTranslator.translate(defaultSetting);
    }

    public List<SettingDescriptor> getDefaultSettingList(String[] names) {
        List<GlobalSettingsEntity> defaultSettingList = globalSettingsRepo.findByNameIn(Arrays.asList(names));
        if(defaultSettingList == null){
            return new ArrayList<>();
        }
        return settingsTranslator.translate(defaultSettingList);
    }

    public List<MakeModelYear> getAllMMYs() {
        Iterable<MMYEntity> allMMYs = mmyRepo.findAll();
        if(allMMYs == null){
            return new ArrayList<>();
        }
        return mmyTranslator.translate(allMMYs);
    }

    public MakeModelYear getMMY(int year, String make, String model) {
        MMYEntity mmyRepoOne = mmyRepo.findOneByYearAndMakeAndModel(year, make, model);
        if(mmyRepoOne == null){
            return new MakeModelYear();
        }
        return mmyTranslator.translate(mmyRepoOne);
    }

    public List<MakeModelYear> getMMYList(List<Long> ids) {
        List<MMYEntity> mmyList = mmyRepo.findByIdIn(ids);
        if(mmyList == null){
            return new ArrayList<>();
        }
        return mmyTranslator.translate(mmyList);
    }
/*POST Methods*/
    public List<SettingDescriptor> createDefaultSettingsList(List<SettingDescriptor> defaultSettings) {
        if(defaultSettings == null || defaultSettings.isEmpty()){
            throw new PixException(HttpStatus.BAD_REQUEST, "Settings array is empty or null", "UTIL-???");
        }
        globalSettingsRepo.deleteAllSettings();
        List<GlobalSettingsEntity> newSettingsList = reverseSettingsTranslator.translate(defaultSettings);
        if(newSettingsList == null){
            throw new PixException(HttpStatus.BAD_REQUEST, "Settings entity list is null", "UTIL-???");
        }
        this.globalSettingsRepo.save(newSettingsList);
        return defaultSettings;
    }

    public List<MakeModelYear> createMMYList(List<MakeModelYear> makeModelYears) {
        // remove old MMY list
        if(makeModelYears == null || makeModelYears.isEmpty()){
            throw new PixException(HttpStatus.BAD_REQUEST, "MakeModelYears array is empty or null", "UTIL-001");
        }
        mmyRepo.deleteAllMMYs();

        List<MMYEntity> newMMYList = reverseMMYTranslator.translate(makeModelYears);
        if(newMMYList == null){
            throw new PixException(HttpStatus.BAD_REQUEST, "MMY entity list is null", "UTIL-001");
        }
        this.mmyRepo.save(newMMYList);
        return makeModelYears;
    }
/*PUT Methods*/
    @Transactional
    public List<SettingDescriptor> addDefaultSettings(List<SettingDescriptor> defaultSettings) {
        if(defaultSettings == null){
            throw new PixException(HttpStatus.BAD_REQUEST, "DefaultSettings list is null", "UTIL-001");
        }
        ArrayList<String> names = new ArrayList<>();
        for (SettingDescriptor setting: defaultSettings) {
            names.add(setting.getName());
        }
        this.globalSettingsRepo.deleteByNameIn(names);
        List<GlobalSettingsEntity> newSettings = new ArrayList<>();
        //loop through all new settings
        for (SettingDescriptor defaultSetting : defaultSettings) {
            newSettings.add(new GlobalSettingsEntity(defaultSetting.getCategory(), defaultSetting.getName(),
                    defaultSetting.getType(), defaultSetting.getMinValue(), defaultSetting.getMaxValue(),
                    defaultSetting.getSelectedValue()));
        }
        //save
        this.globalSettingsRepo.save(newSettings);
        //return
        return this.settingsTranslator.translate(newSettings);
    }

    public List<MakeModelYear> addMMYs(List<MakeModelYear> makeModelYears) {
        if(makeModelYears == null || makeModelYears.isEmpty()) {
            throw new PixException(HttpStatus.BAD_REQUEST, "MMY list is null or empty", "UTIL-???");
        }
        List<MMYEntity> mmyEntityList = reverseMMYTranslator.translate(makeModelYears);
        if(mmyEntityList == null || mmyEntityList.isEmpty()){
            throw new PixException(HttpStatus.BAD_REQUEST, "MMY Entity list is null or empty", "UTIL-???");
        }
        mmyRepo.save(mmyEntityList);
        return makeModelYears;
    }
/*DELETE Methods*/
    @Transactional
    public boolean deleteDefaultSettings(List<String> names) {
        globalSettingsRepo.deleteByNameIn(names);
        return true;
    }

    @Transactional
    public boolean deleteMMYs(List<Long> ids) {
        this.mmyRepo.deleteByIdIn(ids);
        return true;
    }
}
