package com.gm.gsmc.pix.utility.translator;

import com.gm.gsmc.pix.general.dao.AbstractModelTranslator;
import com.gm.gsmc.pix.model.vehicle.MakeModelYear;
import com.gm.gsmc.pix.utility.jpa.model.MMYEntity;
import org.springframework.stereotype.Service;

@Service
public class MMYTranslator extends AbstractModelTranslator<MMYEntity, MakeModelYear> {
    @Override
    public MakeModelYear translate(MMYEntity original) {

        MakeModelYear mmy = new MakeModelYear();
        mmy.setYear(original.getYear());
        mmy.setMake(original.getMake());
        mmy.setModel(original.getModel());
        
        // TODO: Are the following properties needed in MakeModelYear class (they don't exist currently)
/*        mmy.setVehicleProgramCode(original.getVehicleProgramCode());
        mmy.setVehMake(original.getVehMake());
        mmy.setVehModel(original.getVehModel());
        mmy.setRadioGenId(original.getRadioGenId());
        mmy.setNgiCapable(original.isNgiCapable());
        mmy.setInfo3Capable(original.isInfo3Capable());*/

        return mmy;
    }
}
