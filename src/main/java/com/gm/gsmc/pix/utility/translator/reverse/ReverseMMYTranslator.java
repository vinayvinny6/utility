package com.gm.gsmc.pix.utility.translator.reverse;

import com.gm.gsmc.pix.general.dao.AbstractModelTranslator;
import com.gm.gsmc.pix.model.vehicle.MakeModelYear;
import com.gm.gsmc.pix.utility.jpa.model.MMYEntity;
import org.springframework.stereotype.Service;

@SuppressWarnings("Duplicates")
@Service
public class ReverseMMYTranslator extends AbstractModelTranslator<MakeModelYear, MMYEntity> {
    @Override
    public MMYEntity translate(MakeModelYear original) {

        MMYEntity mmyEntity = new MMYEntity();
        mmyEntity.setMake(original.getMake());
        mmyEntity.setModel(original.getModel());
        mmyEntity.setYear(original.getYear());

        // TODO: Are the following properties needed in MakeModelYear class (they don't exist currently)
/*
        mmyEntity.setVehicleProgramCode(original.getVehicleProgramCode());
        mmyEntity.setVehMake(original.getVehMake());
        mmyEntity.setVehModel(original.getVehModel());
        mmyEntity.setRadioGenId(original.getRadioGenId());
        mmyEntity.setInfo3Capable(original.isInfo3Capable());
        mmyEntity.setNgiCapable(original.isNgiCapable());
*/

        return mmyEntity;
    }
}
