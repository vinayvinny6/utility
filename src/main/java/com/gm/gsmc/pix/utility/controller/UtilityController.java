package com.gm.gsmc.pix.utility.controller;

import com.gm.gsmc.pix.api.service.UtilityService;
import com.gm.gsmc.pix.model.settings.SettingDescriptor;
import com.gm.gsmc.pix.model.vehicle.MakeModelYear;
import com.gm.gsmc.pix.utility.manager.UtilityManager;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping ("/utility")
@Api(description = "Utility Service - Manages Utility specific data for all services that need to use it")
public class UtilityController implements UtilityService {

    private final UtilityManager utilityManager;

    @Autowired
    public UtilityController(UtilityManager utilityManager)
    {
        this.utilityManager = utilityManager;
    }

    @Override
    @ApiOperation(value = "Grabs a list of all default settings in the Master List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful response"),
            @ApiResponse (code = 400, message = "There are no valid settings to return from the Master list"),
            @ApiResponse (code = 500, message = "Internal system error") })
    @GetMapping (value ="/defaultSettings/all")
    public List<SettingDescriptor> getAllDefaultSettings(){
        return utilityManager.getAllDefaultSettings();
    }

    @Override
    @ApiOperation(value = "Grabs a select default setting from the Master List by a list of Setting name")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful response"),
            @ApiResponse (code = 400, message = "The name provided does not exist"),
            @ApiResponse (code = 500, message = "Internal system error") })
    @GetMapping (value ="/defaultSettings/{name}")
    public SettingDescriptor getDefaultSetting(
            @PathVariable("name")
            @ApiParam(value = "The name of a setting in the Master List")String name){
        return utilityManager.getDefaultSetting(name);
    }

    @Override
    @ApiOperation(value = "Grabs a list of select default settings from the Master List by Setting name")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful response"),
            @ApiResponse (code = 400, message = "One or more names requested is invalid or does not exist"),
            @ApiResponse (code = 500, message = "Internal system error") })
    @GetMapping (value ="/defaultSettings")
    public List<SettingDescriptor> getDefaultSettingList(
            @RequestParam("names")
            @ApiParam("A list of setting names within the Settings Master List")
            String[] names){
        return utilityManager.getDefaultSettingList(names);
    }

    @Override
    @ApiOperation(value = "Grabs all Make-Model-Years from the Master List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful response"),
            @ApiResponse (code = 400, message = "The request returned null"),
            @ApiResponse (code = 500, message = "Internal system error") })
    @GetMapping (value ="/makeModelYears/all")
    public List<MakeModelYear> getAllMMYs() {
        return utilityManager.getAllMMYs();
    }

    @Override
    @ApiOperation(value = "Grabs one Make-Model-Year from the Master List by id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful response"),
            @ApiResponse (code = 400, message = "The id specified does not exist or was null"),
            @ApiResponse (code = 500, message = "Internal system error") })
    @GetMapping (value ="/makeModelYears/{year}_{make}_{model}")
    public MakeModelYear getMMY(@PathVariable("year") int year,
                                @PathVariable("make") String make,
                                @PathVariable("model") String model) {
        return utilityManager.getMMY(year,make,model);
    }

    @Override
    @ApiOperation(value = "Grabs a list of select Make-Model-Years from the Master List by ids")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful response"),
            @ApiResponse (code = 400, message = "One or more ids requested are invalid or do not exist"),
            @ApiResponse (code = 500, message = "Internal system error") })
    @GetMapping (value ="/makeModelYears")
    public List<MakeModelYear> getMMYList(@RequestParam("ids") List<Long> ids) {
        return utilityManager.getMMYList(ids);
    }

}
