package com.gm.gsmc.pix.utility.controller;

import com.gm.gsmc.pix.api.admin.service.UtilityAdminService;
import com.gm.gsmc.pix.model.settings.SettingDescriptor;
import com.gm.gsmc.pix.model.vehicle.MakeModelYear;
import com.gm.gsmc.pix.utility.manager.UtilityManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/admin")
@Api(description = "Utility Service - Manages Utility specific data for all services that need to use it")
public class UtilityAdvisorUIController implements UtilityAdminService{
    private final UtilityManager utilityManager;

    @Autowired
    public UtilityAdvisorUIController(UtilityManager utilityManager)
    {
        this.utilityManager = utilityManager;
    }

    @Override
    @ApiOperation(value = "Creates a new list of default settings")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful response"),
            @ApiResponse (code = 400, message = "There are no valid settings to send to the Master list"),
            @ApiResponse (code = 500, message = "Internal system error") })
    @PostMapping(value ="/defaultSettings/create", produces = { "application/json" })
    public List<SettingDescriptor> createDefaultSettingsList(@RequestBody List<SettingDescriptor> defaultSettings){
        return utilityManager.createDefaultSettingsList(defaultSettings);
    }

    @Override
    @ApiOperation(value = "Add a single DefaultSetting that was not added to Master List on initial creation")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful response"),
            @ApiResponse (code = 400, message = "Specified MMY object is null or not in the correct format"),
            @ApiResponse (code = 500, message = "Internal system error") })
    @PutMapping (value ="/defaultSettings/add", produces = { "application/json" })
    public List<SettingDescriptor> addDefaultSetting(@RequestBody List<SettingDescriptor> defaultSettings){
        return utilityManager.addDefaultSettings(defaultSettings);
    }

    @Override
    @ApiOperation(value = "Creates a new Master List of Make-Model-Year with NGI/Info3 capabilities")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful response"),
            @ApiResponse (code = 400, message = "There are no valid settings to return from the Master list"),
            @ApiResponse (code = 500, message = "Internal system error") })
    @PostMapping (value ="/makeModelYears/create", produces = { "application/json" })
    public List<MakeModelYear> createMMYList(@RequestBody List<MakeModelYear> makeModelYears){
        return utilityManager.createMMYList(makeModelYears);
    }

    @Override
    @ApiOperation(value = "Add a single MMY that was not added to Master List on initial creation")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful response"),
            @ApiResponse (code = 400, message = "Specified MMY object is null or not in the correct format"),
            @ApiResponse (code = 500, message = "Internal system error") })
    @PutMapping (value ="/makeModelYears/add", produces = { "application/json" })
    public List<MakeModelYear> addMakeModelYear(@RequestBody List<MakeModelYear> makeModelYears){
        //if MMY added in, give a Success response
        return utilityManager.addMMYs(makeModelYears);
    }

    @Override
    @ApiOperation(value = "Removes select MMYs from the MMY Master List in case mistakes were made on initial list creation")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful response"),
            @ApiResponse (code = 400, message = "Specified MMY object does not exist or cannot be deleted"),
            @ApiResponse (code = 500, message = "Internal system error") })
    @DeleteMapping (value ="/makeModelYears/delete", produces = { "application/json" })
    public boolean deleteMMYs(@RequestParam("ids") List<Long> ids) {
        return utilityManager.deleteMMYs(ids);
    }

    @Override
    @ApiOperation(value = "Deletes a select list of settings that may have incorrectly been imported upon the Master List creation")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful response"),
            @ApiResponse (code = 400, message = "Specified global settings do not exist or cannot be removed"),
            @ApiResponse (code = 500, message = "Internal system error") })
    @DeleteMapping (value ="/defaultSettings/delete", produces = { "application/json" })
    public boolean deleteDefaultSettings(@RequestParam("names") List<String> names){
        return utilityManager.deleteDefaultSettings(names);
    }
}
