package com.gm.gsmc.pix.utility.config;

import org.springframework.context.annotation.Configuration;

import com.gm.gsmc.pix.general.config.AbstractSwaggerBasicConfiguration;

/**
 * @author TZVZCY
 */
@Configuration
public class SwaggerConfiguration extends AbstractSwaggerBasicConfiguration {

    @Override
    protected String getName() {
        return "Utility";
    }

    @Override
    protected String getDescription() {
        return "Utility Service";
    }

    @Override
    protected String getControllerBasePackage() {
        return "com.gm.gsmc.pix.utility.controller";
    }

}
