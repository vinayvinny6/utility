package com.gm.gsmc.pix.utility.jpa;

import com.gm.gsmc.pix.utility.jpa.model.MMYEntity;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;

public interface MMYRepository extends CrudRepository<MMYEntity, Long> {

    List<MMYEntity> findByIdIn(Collection<Long> ids);

    @Modifying
    @Transactional
    @Query("DELETE FROM MMYEntity")
    void deleteAllMMYs();

    void deleteByIdIn(List<Long> ids);
//    List<Country> findByCountry(Long countryId);

    MMYEntity findOneByYearAndMakeAndModel(int year, String make, String model);
}
