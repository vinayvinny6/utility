package com.gm.gsmc.pix.utility.jpa.model;

import com.gm.gsmc.pix.model.settings.SettingCategory;
import com.gm.gsmc.pix.model.settings.SettingType;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
@Data
@NoArgsConstructor

@Entity
@Table(name = "UTILITY_GLOBAL_SETTINGS")
public class GlobalSettingsEntity {

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    private SettingCategory category;

    private String name;

    @Enumerated (EnumType.STRING)
    private SettingType type;

    private int minValue;
    private int maxValue;
    private String defaultValue;

    public GlobalSettingsEntity(SettingCategory category, String name, SettingType type, String defaultValue) {
        this.category = category;
        this.name = name;
        this.type = type;
        this.defaultValue = defaultValue;
    }

    public GlobalSettingsEntity(SettingCategory category, String name, SettingType type, int minValue, int maxValue, String defaultValue) {
        this.category = category;
        this.name = name;
        this.type = type;
        this.minValue = minValue;
        this.maxValue = maxValue;
        this.defaultValue = defaultValue;
    }
}
