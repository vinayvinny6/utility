package com.gm.gsmc.pix.utility.jpa.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * @author YZ70WZ
 */
@Data
@NoArgsConstructor

@Entity
@Table (name = "UTILITY_MMY")
public class MMYEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String vehicleKey;

    private String make;
    private String model;
    private int year;
    private String vehicleProgramCode;
    private String vehMake;
    private String vehModel;

    private int radioGenId;
    private boolean ngiCapable;
    private boolean info3Capable;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @Column(name = "country_flag")
    private List<Country> country;

    public MMYEntity(String make, String model, int year) {
        vehicleKey = getVehicleKey();
        this.make = make;
        this.model = model;
        this.year = year;
    }

    private String getVehicleKey() {
        return getVehMake() + "_" + getVehModel() + "_" + getYear();
    }
}
