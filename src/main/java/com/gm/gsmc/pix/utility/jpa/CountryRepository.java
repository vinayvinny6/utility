package com.gm.gsmc.pix.utility.jpa;

import com.gm.gsmc.pix.model.CountryIsoCode;
import com.gm.gsmc.pix.model.LanguageCode;
import com.gm.gsmc.pix.model.Region;
import com.gm.gsmc.pix.utility.jpa.model.Country;
import org.springframework.data.repository.CrudRepository;

public interface CountryRepository extends CrudRepository<Country, Long> {
    void findByRegion(Region region);
    void findByCountry(CountryIsoCode country);
    void findByLanguage(LanguageCode language);
}
