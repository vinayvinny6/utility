package com.gm.gsmc.pix.utility;

import com.gm.gsmc.pix.model.settings.SettingCategory;
import com.gm.gsmc.pix.model.settings.SettingType;
import com.gm.gsmc.pix.utility.jpa.GlobalSettingsRepository;
import com.gm.gsmc.pix.utility.jpa.model.GlobalSettingsEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.List;

/**
 * @author TZVZCY
 */
@SpringBootApplication
@EnableSwagger2
@ComponentScan
//@EnableDiscoveryClient
public class UtilityApplication {

    private static final Logger log = LoggerFactory.getLogger(UtilityApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(UtilityApplication.class, args);
    }

    /*@Bean
    public CommandLineRunner demo(GlobalSettingsRepository globalSettingsRepo) {
        return (args) -> {

            List<GlobalSettingsEntity> settingsList1 = new ArrayList<>();

            GlobalSettingsEntity setting1 = new GlobalSettingsEntity(SettingCategory.VehicleSetting, "Radio Level", SettingType.INT, 0, 30, "15");

            GlobalSettingsEntity setting2 = new GlobalSettingsEntity(SettingCategory.VehicleSetting, "Radio", SettingType.STRING, 0, 0, "SCM");

            GlobalSettingsEntity setting3 = new GlobalSettingsEntity(SettingCategory.VehicleSetting, "Bose Sound System", SettingType.BOOLEAN, 0, 30, "true");


            globalSettingsRepo.save(setting1);
            globalSettingsRepo.save(setting2);
            globalSettingsRepo.save(setting3);

            settingsList1.add(setting1);
            settingsList1.add(setting2);
            settingsList1.add(setting3);

            globalSettingsRepo.save(settingsList1);

            // fetch all global settings
            log.info("Settings found with findAll():");
            log.info("-------------------------------");
            for (GlobalSettingsEntity setting : globalSettingsRepo.findAll()) {
                log.info(setting.getName());
            }
            log.info("");
        };
    }*/
}
