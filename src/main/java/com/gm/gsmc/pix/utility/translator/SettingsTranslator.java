package com.gm.gsmc.pix.utility.translator;

import com.gm.gsmc.pix.general.dao.AbstractModelTranslator;
import com.gm.gsmc.pix.model.settings.SettingDescriptor;
import com.gm.gsmc.pix.utility.jpa.model.GlobalSettingsEntity;
import org.springframework.stereotype.Service;

@Service
public class SettingsTranslator extends AbstractModelTranslator<GlobalSettingsEntity, SettingDescriptor> {

    @Override
    public SettingDescriptor translate(GlobalSettingsEntity original) {
        SettingDescriptor setting = new SettingDescriptor();
        setting.setCategory(original.getCategory());
        setting.setName(original.getName());
        setting.setType(original.getType());
        setting.setMinValue(original.getMinValue());
        setting.setMaxValue(original.getMaxValue());
        setting.setSelectedValue(original.getDefaultValue());

        return setting;
    }
}
