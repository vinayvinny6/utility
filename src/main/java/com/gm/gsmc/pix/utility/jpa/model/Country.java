package com.gm.gsmc.pix.utility.jpa.model;

import com.gm.gsmc.pix.model.CountryIsoCode;
import com.gm.gsmc.pix.model.LanguageCode;
import com.gm.gsmc.pix.model.Region;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * @author TZVZCY
 */
@Data
@NoArgsConstructor

@Entity
@Table(name = "UTILITY_COUNTRY")
public class Country {
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    private CountryIsoCode country;
    @Enumerated(EnumType.STRING)
    private Region region;
    @Enumerated(EnumType.STRING)
    private LanguageCode language;


    public Country(CountryIsoCode country, LanguageCode language, Region region) {
//        this.name = name;
        this.country = country;
        this.region = region;
        this.language = language;
    }
}




