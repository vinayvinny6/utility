package com.gm.gsmc.pix.utility.translator.reverse;

import com.gm.gsmc.pix.general.dao.AbstractModelTranslator;
import com.gm.gsmc.pix.model.settings.SettingDescriptor;
import com.gm.gsmc.pix.utility.jpa.model.GlobalSettingsEntity;
import org.springframework.stereotype.Service;

@Service
public class ReverseSettingsTranslator extends AbstractModelTranslator<SettingDescriptor, GlobalSettingsEntity> {
    @Override
    public GlobalSettingsEntity translate(SettingDescriptor original) {
        GlobalSettingsEntity globalSettingsEntity = new GlobalSettingsEntity();
        globalSettingsEntity.setCategory(original.getCategory());
        globalSettingsEntity.setName(original.getName());
        globalSettingsEntity.setType(original.getType());
        globalSettingsEntity.setMinValue(original.getMinValue());
        globalSettingsEntity.setMaxValue(original.getMaxValue());
        globalSettingsEntity.setDefaultValue(original.getSelectedValue());

        return globalSettingsEntity;
    }
}
