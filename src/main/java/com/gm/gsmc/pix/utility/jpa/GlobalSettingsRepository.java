package com.gm.gsmc.pix.utility.jpa;

import com.gm.gsmc.pix.utility.jpa.model.GlobalSettingsEntity;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;

@Service
public interface GlobalSettingsRepository extends CrudRepository<GlobalSettingsEntity, Long> {

    GlobalSettingsEntity findOneByName(String name);
    List<GlobalSettingsEntity> findByNameIn(Collection<String> names);

    void deleteByNameIn(Collection<String> names);

    @Modifying
    @Transactional
    @Query("DELETE FROM GlobalSettingsEntity")
    void deleteAllSettings();


}
